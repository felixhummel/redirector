#!/bin/bash
set -euo pipefail

STATUS_CODE=${STATUS_CODE:-307}

cat <<EOF > /etc/nginx/conf.d/default.conf
server {
  listen 80 default_server;

  location / {
    return $STATUS_CODE $URI;
  }
}
EOF
nginx -t

exec "$@"
