# Usage
```
docker run -d --rm \
  -p 8123:80 \
  -e URI=https://example.com \
  --name redirector \
  registry.gitlab.com/felixhummel/redirector:1.0.2

curl localhost:8123
```

You may also set the [HTTP status code][1]. For a permanent
redirect you would use:
```
docker run -d --rm \
  -p 8123:80 \
  -e URI=https://example.com \
  -e STATUS_CODE=301 \
  --name redirector \
  registry.gitlab.com/felixhummel/redirector:1.0.2
```

[1]: https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#3xx_Redirection

# Development
```
make
curl localhost:8123
```
