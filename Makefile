.PHONY: roundtrip
roundtrip: build run

.PHONY: build
build:
	docker build -t redirector:1.0.2 .

.PHONY: run
run:
	docker run --rm -p 8123:80 -e URI=https://example.com redirector:1.0.2
